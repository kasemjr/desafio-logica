var StringUtil = (function () {

    /**
     * Remove caracteres como quebra de linha e espaços em branco no inicio e final da string
     * 
     * @returns String Valor "limpo"
     */
    this.clearValue = function (value) {
        if (value) {
            return ['', value]
                .join('')
                .replace(/(\r\n|\n|\r)/gm, '')
                .trim();
        }
        return null;
    };

    return this;

})();

var NumberUtil = (function () {

    /**
     * Converte o valor para inteiro
     * 
     * @returns number
     */
    this.toInt = function (value) {

        var i = parseInt(value);

        if (isNaN(i)) {
            return null;
        }

        return i;

    };

    /**
     * Corrige o valor de um ponto flutuante
     * 
     * @returns {Number} Valor corrigido
     */
    this.fixFloat = function (value) {
        return Math.round(value * 100) / 100;
    };

    return this;

})();

var CurrencyService = function (strategy) {

    this.strategy = strategy;

};

CurrencyService.prototype = {

    /**
     * Altera para a estratégia da moeda de uma determinada região
     * @param {*} strategy 
     */
    setStrategy: function (strategy) {
        this.strategy = strategy;
    },

    /**
     * Converte o valor da moeda para ponto flutuante
     * 
     * @param {String} value Valor da moeda formatado no padrão da região
     * 
     * @returns {Number} Valor em ponto flutuante
     */
    toFloat: function (value) {
        return this.strategy.toFloat(value);
    },

    /**
     * Formata o valor numérico para o formato da moeda da região
     * 
     * @param {Number} value 
     * 
     * @returns {String} Valor formatado.
     */
    format: function (value) {
        return this.strategy.format(value);
    }

};

var BrazilianCurrencyStrategy = function () {

    /**
     * Remove os caracteres que simbolizam a moeda
     * 
     * @param {String} value Valor com o simbolo da moeda. Ex: R$ 1.000,00
     * 
     * @returns {String} Valor limpo: Ex: 1.000,00
     */
    var clear = function (value) {
        var matches = new RegExp(/[0-9.,].*/g).exec(value);

        if (!matches) {
            return null;
        }

        return matches[0];
    };

    /**
     * @param {String} value Valor com o simbolo da moeda. Ex: R$ 1.234,56
     * 
     * @returns {Number} Valor convertido para float: Ex: 1234.56
     */
    this.toFloat = function (moneyValue) {
        var value = clear(moneyValue);

        value = value
            .replace('.', '')
            .replace(',', '.');

        return parseFloat(value);
    };

    /**
     * Formata o valor numérico para o formato da moeda da região
     * 
     * @param {Number} value 
     * 
     * @returns {String} Valor formatado. Ex: R$ 1.234,56
     */
    this.format = function (value) {
        var v = value || 0;
        return `R$ ${v.toLocaleString('pt-BR', { minimumFractionDigits: 2 })}`;
    };

};

var DateService = function (location) {
    this.strategy = location;
};

DateService.prototype = {
    /**
     * Obtem o nome da localidade
     * 
     * @returns {String}
     */
    getLocale: function () {
        this.strategy.getLocale();
    },

    /**
     * Obtem o nome do mês da região
     * 
     * @param {Number} month Número do mês. Ex: 1 (para janeiro), 12 (para dezembro)
     * 
     * @returns {String} O nome do mês da região
     */
    getMonthName: function (month) {
        var date = new Date();
        date.setDate(1);
        date.setMonth(month - 1);
        return this.strategy.getMonthName(date);
    }
};

var BrazilianDateStrategy = function () {
    this.getLocale = function () {
        return 'pt-BR';
    };
    this.getMonthName = function (date) {
        return date.toLocaleString(this.getLocale(), { month: 'long' });
    };
};

(function () {

    /**
     * Inicializa a "aplicação"
     */
    var start = function () {

        var dadosFaturaRaw = obterDadosFatura();

        var dadosFatura = normalizarDadosFatura(dadosFaturaRaw);

        var faturaConsolidada = consolidarDadosFatura(dadosFatura);

        exibirFaturaConsolidada(faturaConsolidada);

    };

    /**
     * Obtem os valores puros da tabela de fatura 
     * 
     * @returns {{lancamento: String, valor: String, mes: String}[]} Valores obtidos da tabela de fatura
     */
    var obterDadosFatura = function () {

        var rows = document
            .getElementById('tb_fatura')
            .querySelectorAll('tbody tr');

        var dados = [];

        rows.forEach(function (row) {

            var columns = row.querySelectorAll('td');

            var lancamento = StringUtil.clearValue(columns[0].innerText);
            var valor = StringUtil.clearValue(columns[1].innerText);
            var mes = StringUtil.clearValue(columns[2].innerText);

            dados.push({
                lancamento: lancamento,
                valor: valor,
                mes: mes
            });

        });

        return dados;

    };

    /**
     * Normaliza os valores dos dados da fatura
     * 
     * @param {{lancamento: String, valor: String, mes: String}[]} dadosFaturaRaw 
     * 
     * @returns {{lancamento: String, valor: Number, mes: Number}[]}
     */
    var normalizarDadosFatura = function (dadosFaturaRaw) {

        var fatura = [];

        var currency = new CurrencyService(new BrazilianCurrencyStrategy());

        dadosFaturaRaw.forEach(function (dados) {

            fatura.push({
                lancamento: dados.lancamento,
                valor: currency.toFloat(dados.valor),
                mes: NumberUtil.toInt(dados.mes)
            });

        });

        return fatura;

    };

    /**
     * Consolida os dados da fatura
     * 
     * @param {{lancamento: String, valor: Number, mes: Number}[]} dadosFatura
     * 
     * @returns {{mes: Number, totalGasto: Number}[]} Fatura consolidada
     */
    var consolidarDadosFatura = function (dadosFatura) {

        var consolidadoMensal = dadosFatura
            .reduce(function (mes, lancamento) {
                var valorAcumulado = mes[lancamento.mes] || 0;
                valorAcumulado += lancamento.valor;
                mes[lancamento.mes] = NumberUtil.fixFloat(valorAcumulado);
                return mes;
            }, {});

        var consolidado = [];

        Object
            .keys(consolidadoMensal)
            .forEach(function (mes) {
                consolidado.push({
                    mes: NumberUtil.toInt(mes),
                    totalGasto: consolidadoMensal[mes]
                });
            });

        return consolidado;

    };

    /**
     * Exibe os dados consolidados da fatura na tabela
     * 
     * @param {{mes: Number, totalGasto: Number}[]} faturaConsolidada
     */
    var exibirFaturaConsolidada = function (faturaConsolidada) {

        var tabelaConsolidadaEl = document.getElementById('tb_consolidado');

        var htmlDadosConsolidados = [];

        var currency = new CurrencyService(new BrazilianCurrencyStrategy());
        var dateService = new DateService(new BrazilianDateStrategy());

        faturaConsolidada
            .forEach(function (consolidadoMes) {
                var mes = dateService.getMonthName(consolidadoMes.mes);
                var gastos = currency.format(consolidadoMes.totalGasto);

                htmlDadosConsolidados.push('<tr>');
                htmlDadosConsolidados.push(`<td class="text-capitalize">${mes}</td>`);
                htmlDadosConsolidados.push(`<td>${gastos}</td>`);
                htmlDadosConsolidados.push('</tr>');
            });

        tabelaConsolidadaEl
            .querySelector('tbody')
            .innerHTML = htmlDadosConsolidados.join('');

    };

    document.addEventListener('DOMContentLoaded', start);

})();